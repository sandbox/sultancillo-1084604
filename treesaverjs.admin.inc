<?php
/**
 * @file
 * Administration page callbacks for the Treesaver module.
 */
 
 /**
 * Display the Treesaver settings form.
 */
function treesaverjs_admin_settings_form($form_state) {
  $form = array();

  // Treesaver path:
  $form['treesaverjs_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to Treesaver javascript library'),
    '#default_value' => variable_get('treesaverjs_path', TREESAVER_DEFAULT_PATH),
    '#description' => t('Enter the location of the <a href="!url" target="_blank">Treesaver javascript library</a>. It is recommended to use %path.', array('!url' => 'http://treesaverjs.com', '%path' => TREESAVER_DEFAULT_PATH)),
  );
  
  
 if ($files = variable_get('treesaverjs_files', NULL)) {
    $form['treesaverjs_files'] = array(
      '#prefix' => '<div class="messages status">',
      '#suffix' => '</div>',
      '#markup' => t('Library detected, using @js', array('@js' => $files['js'])),
    );
  }

  $settings = variable_get('treesaverjs_settings', array());

  // Activation:
  $data['activation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Activation'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $data['activation']['activation_type'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Treesaver on specific pages'),
    '#options' => array(
      'exclude' => t('Enable on every page except the listed pages.'),
      'include' => t('Enable on only the listed pages.'),
    ),
    '#default_value' => isset($settings['activation']['activation_type']) ? $settings['activation']['activation_type'] : 'exclude',
  );
  if (user_access('use PHP for block visibility')) {
    $data['activation']['activation_type']['#options']['php'] = t('Enable if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
  }
  $data['activation']['activation_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => isset($settings['activation']['activation_pages']) ? $settings['activation']['activation_pages'] : "admin*\nnode/add/*\nnode/*/edit",
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
  );

  $form['data'] = $data;
  $form['data']['#tree'] = TRUE;

  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults') );

  return $form;
}

/**
 * Validation handler for the Treesaver settings form.
 */
function treesaverjs_admin_settings_form_validate($form, &$form_state) {

  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == t('Reset to defaults')) {
    return; // Skip validation if Reseting to defaults
  }

  // Check treesaverjs_path for .js files.
  $path =  rtrim($form_state['values']['treesaverjs_path'], '/ ');
  $files = _detect_treesaverjs_files($path);

  if (empty($files['js'])) {
    form_set_error('treesaverjs_path', t('No Treesaver javascript library found in @path',array('@path' => $path)));
  }
}

/**
 * Submit handler for the Treesaver settings form.
 */
function treesaverjs_admin_settings_form_submit($form, &$form_state) {
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == t('Reset to defaults')) {
    variable_del('treesaverjs_settings');
    variable_del('treesaverjs_path');
    variable_del('treesaverjs_files');
    drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  else {

    // Remove trailing slash from path
    $path =  rtrim($form_state['values']['treesaverjs_path'], '/ ');

    variable_set('treesaverjs_path', $path);
    variable_set('treesaverjs_files', _detect_treesaverjs_files($path));

    _save_treesaverjs_settings($form_state);

    drupal_set_message(t('The configuration options have been saved.'));
  }
}

/**
 * Save settings into 'treesaverjs_settings' variable
 */
function _save_treesaverjs_settings($form_state) {
    variable_set('treesaverjs_settings', $form_state['values']['data']);
}


/*****************************************************************************
 * HELPER FUNCTIONS
 *****************************************************************************/

/**
 * Flatten an array, preserving its keys.
 */
function treesaverjs_array_flatten($array) {
  $result = array();
  if (is_array($array)) {
    foreach ($array as $key => $value) {
      if (is_array($value)) {
        $result += treesaverjs_array_flatten($value);
      }
      else {
        $result[$key] = $value;
      }
    }
  }
  return $result;
}

/**
 * Detect needed script and stylesheet in specified path
 */
function _detect_treesaverjs_files($path) {
  $js_files = glob(getcwd() . DIRECTORY_SEPARATOR . $path .  DIRECTORY_SEPARATOR . 'treesaver-*.js');
  //$css_files = glob(getcwd() . DIRECTORY_SEPARATOR . $path .  DIRECTORY_SEPARATOR . 'jquery.*.css');

  $files = array(
    'js' => basename(array_shift($js_files)),
   // 'css' => basename(array_shift($css_files)),
  );

  return $files;
}